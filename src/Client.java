import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String args[]) {
        String msg = "软件构件技术课程例子-发送测试数据，兔子几条腿？\n\n";
        try {
            // 创建⼀个Socket，跟本机的8080端⼝连接
            Socket socket = new Socket("127.0.0.1", 8080);
            // 使用Socket创建PrintWriter和BufferedReader进⾏读写数据
            PrintWriter pw = new PrintWriter(socket.getOutputStream());
            BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // 发送数据
            pw.println(msg);
            pw.flush();
            // 接收数据
            String oString="";
            String line = is.readLine();
            while(!(line==null))
            {
                oString=oString+line;
                line=is.readLine();
            }
            System.out.println("从服务器返回 server的数据: " + oString);
            // 关闭资源
            pw.close();
            is.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}