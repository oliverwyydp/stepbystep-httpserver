package cn.edu.bbc.copmpter.servlet;

import java.io.File;
import cn.edu.bbc.copmpter.htmlutils.HtmlProcess;
import cn.edu.bbc.copmpter.httpaction.Request;
import cn.edu.bbc.copmpter.httpaction.Response;
public class LoginServlet extends Servlet {
    @Override
    public void doGet(Request request, Response response) throws Exception {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        //这列可以做什么？数据库的操作啊。啊啊啊啊，原来Servlet就这么简单
		HtmlProcess h=new HtmlProcess();
        if (name!= null && password !=null && name.equals("oliver") 
            && password.equals("olivertest")) {
        	String mainIndex=h.toHtmlString(new File("src/index.html"));
        	response.print(mainIndex);
        }
        else {
        	String failHTML=h.toHtmlString(new File("src/loginfail.html"));
            response.print(failHTML);
        }
    }
    @Override
    public void doPost(Request request, Response reponse) throws Exception {
        doGet(request,reponse);
    }
}
