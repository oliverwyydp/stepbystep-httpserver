package cn.edu.bbc.copmpter.servlet;

import cn.edu.bbc.copmpter.httpaction.Request;
import cn.edu.bbc.copmpter.httpaction.Response;

/**
 *   Servlet抽象类
 */
public abstract class Servlet {

    public void service(Request request, Response reponse) throws Exception {
        this.doGet(request,reponse);
        this.doPost(request,reponse);
    }

    public abstract void doGet(Request request, Response reponse) throws Exception;
    public abstract void doPost(Request request, Response reponse) throws Exception;
}
