package cn.edu.bbc.copmpter.BasicServletFrame;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class MainServer {

	public static void main(String[] args) throws IOException {
			ServerSocket server = new ServerSocket(8080);
			System.out.println("http服务器启动成功....");
			// 多线程处理每个请求
			while (true) {
				Socket client = server.accept(); // 阻塞式等待接收一个请求
				new GeneralServerThread(client).start();
			}
	}
}
