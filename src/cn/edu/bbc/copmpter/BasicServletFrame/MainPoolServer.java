package cn.edu.bbc.copmpter.BasicServletFrame;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import cn.edu.bbc.copmpter.pool.ServicePool;

public class MainPoolServer {
	private static ServicePool<ServerThread> servicePool = new ServicePool<>();

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		ServerSocket server = new ServerSocket(8080);
		System.out.println("http服务器启动成功....");
		// 多线程处理每个请求
		while (true) {
			Socket client = server.accept(); // 阻塞式等待接收一个请求
			servicePool.addJob(new ServerThread(client));
		}
	}
}
