package cn.edu.bbc.copmpter.servletserver;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import cn.edu.bbc.copmpter.servlet.Servlet;

import java.io.File;
import java.util.List;

/**
 *   Servlet工厂
 *          根据url和xml文件创建Servlet
 */
public class ServletFactory {

    //Servlet上下文环境
    private static ServletContext context = new ServletContext();

    //web.xml文件路径
    private static String xmlpath = "src/web.xml";

    private ServletFactory(){}
    /**
     *      读取web.xml文件把servlet和url的关系进行配置存储
     */
    static {
        try {
            //1-获得doucument
            SAXReader saxReader = new SAXReader();
            File f=new File(xmlpath);
            System.out.println(f.getAbsolutePath());
            Document document = saxReader.read(new File(xmlpath));
            
            //2-获得根元素      <web-app>
            Element rootElement = document.getRootElement();

            //3-获得所有子元素
            List<Element> elements = rootElement.elements();

            //4-遍历处理所有子元素
            for (Element e : elements) {
                if ("servlet-mapping".equals(e.getName())) {
                    Element servlet_name = e.element("servlet-name");
                    Element url_pattern = e.element("url-pattern");
                    context.getUrl_map().put(url_pattern.getText(),servlet_name.getText());
                }
                else if ("servlet".equals(e.getName())) {
                    Element servlet_name = e.element("servlet-name");
                    Element servlet_class = e.element("servlet-class");
                    context.getServlet_map().put(servlet_name.getText(),servlet_class.getText());
                }
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    /**
     *  获得Servlet
     */
    public static synchronized Servlet getServlet(String url) throws Exception {
        String servletClass = context.getServlet_map().get(context.getUrl_map().get(url));
        System.out.println(servletClass);
        if (servletClass != null)
            return (Servlet)Class.forName(servletClass).getDeclaredConstructor().newInstance();
        else
            return null;
    }
}
