package cn.edu.bbc.copmpter.servletserver;

import java.io.IOException;
import java.net.Socket;

import cn.edu.bbc.copmpter.httpaction.Request;
import cn.edu.bbc.copmpter.httpaction.Response;
import cn.edu.bbc.copmpter.servlet.Servlet;
public class ServletThread extends Thread {
    private Request request;  //请求
    private Response reponse;   //响应
    private Socket client;
    //初始化request，reponse
    public ServletThread(Socket client) {
        try {
            this.client = client;
            request = new Request(client.getInputStream());
            reponse = new Response(client.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            System.out.println(client.getRemoteSocketAddress()+" 发出请求");
            //浏览器会默认请求网站图标资源，我们这里忽略掉这个请求
            if (request.getUrl().equals("/favicon.ico"))
                return;
            //1-根据请求的url获得Servlet
            System.out.println(request.getUrl());
            Servlet servlet  = ServletFactory.getServlet(request.getUrl());
            //请求资源不存在404
            if (servlet == null){
                reponse.setCode(404);
                reponse.print("");
            }
            //2-执行Servlet
            if (servlet != null){
                servlet.service(request,reponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
