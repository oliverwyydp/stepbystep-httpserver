package cn.edu.bbc.copmpter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThread extends Thread {
	private Socket client;

	public ServerThread(Socket client) {
		super();
		this.client = client;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(client.getRemoteSocketAddress() + " 发出请求");
		BufferedReader is;
		try {
			is = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			StringBuffer inStr = new StringBuffer();
			String inHeader = null;
			String line = null;
			line = is.readLine();
			while (line.length() > 0) {
				inStr.append(line + "\n<br>");
				line = is.readLine();
			}
			inHeader = "我是服务器，收到来自:<br> " + inStr.toString();
			System.out.println(inHeader);
			PrintWriter pw = new PrintWriter(this.client.getOutputStream());
			StringBuffer sb = new StringBuffer();
			sb.append("HTTP/1.1 200 OK \r\n");
			sb.append("Content-Type: text/html \r\n");
			sb.append("\r\n");
			sb.append("<html>\n");
			sb.append("<body>\n");
			sb.append("<h1>" + inHeader + "</h1>\n");
			sb.append("</body>\n");
			sb.append("</html>\n");
			// 创建PrintWriter，用于发送数据
			System.out.println(sb.toString());
			pw.println(sb.toString());
			pw.flush();
			// 关闭资源
			pw.close();
			is.close();
			while(true)
			{

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
