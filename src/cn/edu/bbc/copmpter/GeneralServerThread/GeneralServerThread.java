package cn.edu.bbc.copmpter.GeneralServerThread;

import java.io.File;
import java.io.IOException;
import java.net.Socket;

import cn.edu.bbc.copmpter.htmlutils.HtmlProcess;
import cn.edu.bbc.copmpter.httpaction.Request;
import cn.edu.bbc.copmpter.httpaction.Response;

public class GeneralServerThread extends Thread {
	private Request request; // 请求
	private Response response; // 响应
	private Socket client;

	// 初始化request，reponse
	public GeneralServerThread(Socket client) {
		try {
			this.client = client;
			request = new Request(client.getInputStream());
			response = new Response(client.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			System.out.println(client.getRemoteSocketAddress() + " 发出请求");

			// 浏览器会默认请求网站图标资源，我们这里忽略掉这个请求
			if (request.getUrl().equals("/favicon.ico"))
				return;
			// 1-根据请求的url获得对应的静态文件，这里仅仅是个演示，更深的套路是封装一个httphandler类任务交给他处理
			HtmlProcess h=new HtmlProcess();
			if (request.getUrl().equals("/")) {
				String strHtml = h.toHtmlString(new File("src/index.html"));
				response.print(strHtml);
			} else {
				String str404 = h.toHtmlString(new File("src/404.html"));
				response.print(str404);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
