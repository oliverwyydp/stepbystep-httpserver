package cn.edu.bbc.copmpter.threadtest;

public class ThreadCount extends Thread{

    public ThreadCount() {
        super();
    }
    public ThreadCount(String name)
    {
        super(name);
    }
    @Override
    public void run()
    {
        for (int i=0;i<10;i++)
        {
            System.out.printf("第: %d次执行，---%s\n",i,getName());
            try{
                long sleepTime=(long) (1000*Math.random());
                sleep(sleepTime);
            }catch (InterruptedException e)
            {
                System.out.println(e.getMessage());
            }
        }
        System.out.println("线程："+getName()+"  执行完成！");
    }
}
