package cn.edu.bbc.copmpter;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws Exception {
        ServerSocket server = new ServerSocket(8080);
        System.out.println("本机端口：" + String.valueOf(8080) + "服务已经启动，请通过合适的客户端链接测试");
        // 使用多线程处理每个请求
        while (true) {
            Socket client = server.accept(); // 阻塞式等待接收一个请求
            new ServerThread(client).start();
        }
    }
}
