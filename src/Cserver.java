import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Cserver {
	public static void main(String args[]) {
		try {
			// 创建⼀个ServerSocket监听8080端⼝
			ServerSocket server = new ServerSocket(8080);
			// 等待请求
			System.out.println("本机端口：" + String.valueOf(8080) + "服务已经启动，请通过合适的客户端链接测试");
			while (true) {
				Socket socket = server.accept();
				// 接收到请求后使用socket进⾏通信，创建BufferedReader用于读取数据，
				BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				StringBuffer inStr = new StringBuffer();
				String inHeader = null;
				String line = null;
				line = is.readLine();
				while (line.length() > 0) {
					inStr.append(line + "\r\n<br>");
					line = is.readLine();
				}
				inHeader = "我是服务器，收到来自:<br> " + inStr.toString();
				System.out.println(inHeader);
				PrintWriter pw = new PrintWriter(socket.getOutputStream());
				StringBuffer sb = new StringBuffer();
				sb.append("HTTP/1.1 200 OK \r\n");
				sb.append("Content-Type: text/html \r\n");
				sb.append("\r\n");
				sb.append("<html>\n");
				sb.append("<body>\n");
				sb.append("<h1>" + inHeader + "</h1>\n");
				sb.append("</body>\n");
				sb.append("</html>\n");
				// 创建PrintWriter，用于发送数据
				System.out.println(sb.toString());
				pw.println(sb.toString());
				pw.flush();
				// 关闭资源
				pw.close();
				is.close();
				socket.close();
				while(true)
				{

				}
			}
//			server.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}